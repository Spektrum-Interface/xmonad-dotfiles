#!/bin/sh
_DEVPATH=$(find /sys/devices -name product -exec grep -qi 'dock' {} \; -exec dirname -- {} \; -quit)
if read -r bus < $_DEVPATH/busnum && read -r dev < $_DEVPATH/devnum >/dev/null
then
  echo "Online"
else
  echo "Offline"
fi
